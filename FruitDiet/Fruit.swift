//
//  Fruit.swift
//  FruitDiet
//
//  Created by Adam Preston on 4/28/16.
//  Copyright © 2016 RTI. All rights reserved.
//

import Foundation

class Fruit {
    var name:String?
    var group:String?
    
    init(name: String, group: String) {
        self.name = name
        self.group = group
    }
}