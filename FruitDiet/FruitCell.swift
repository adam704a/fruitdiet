//
//  FruitCell.swift
//  FruitDiet
//
//  Created by Adam Preston on 4/28/16.
//  Copyright © 2016 RTI. All rights reserved.
//

import UIKit

class FruitCell: UICollectionViewCell {

    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
}
